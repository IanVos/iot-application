<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reminder;

use Illuminate\Support\Facades\Validator;

class ReminderController extends Controller
{
    public function getReminder($id = 0){
        // Check if an ID is given
        if($id==0){
            // Get all device data from database and order ascending by ID
            $arr['data'] = Reminder::orderBy('id', 'asc')->get(); 
        }else{
            // Get the first row where the ID matches
            $arr['data'] = Reminder::where('espid', $id)->get();
        }
        // Echo the data in json format
        echo json_encode($arr);
        exit;
    }

    public function newReminder(Request $request){

        $rules = [
			'device_id' => 'required|integer',
			'name' => 'required',
			'time' => 'required',
			'prio' => 'required'
		];
		$validator = Validator::make($request->all(),$rules);
		if ($validator->fails()) {
		}
		else{
            $data = $request->input();
			try{
				$reminder = new Reminder;
				$reminder->espid = $data['device_id'];
				$reminder->name = $data['name'];
				$reminder->time = $data['time'];
				$prio = 0;
				switch ($data['prio']) {
					case 'high':
						$prio = 2;
						break;
					case 'medium':
						$prio = 1;
						break;
					case 'low':
						$prio = 0;
						break;
				}
				$reminder->prio = $prio;

				$reminder->save();
				
			}
			catch(Exception $e){
			}
		}
	}
	
}
