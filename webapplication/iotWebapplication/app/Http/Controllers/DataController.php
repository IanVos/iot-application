<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;

class DataController extends Controller
{

    // Get data from the model and return as json
    public function getDevice($id = 0){
        // Check if an ID is given
        if($id==0){
            // Get all device data from database and order ascending by ID
            $arr['data'] = Device::orderBy('id', 'asc')->get(); 
        }else{
            // Get the first row where the ID matches
            $arr['data'] = Device::where('id', $id)->first();
        }
        // Echo the data in json format
        echo json_encode($arr);
        exit;
    }
}
