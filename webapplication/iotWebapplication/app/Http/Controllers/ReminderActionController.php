<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReminderAction;

class ReminderActionController extends Controller
{
    
    public function getReminderAction($id = 0){
        // Check if an ID is given
        if($id==0){
            // Get all reminder actions from database and order ascending by ID
            $arr['data'] = ReminderAction::orderBy('id', 'asc')->get(); 
        }else{
            // Get All actions from a reminder
            $arr['data'] = ReminderAction::where('reminder_id', $id)->get();
        }
        // Echo the data in json format
        echo json_encode($arr);
        exit;
    }


}
