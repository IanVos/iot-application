<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;
use App\Http\Controllers\ReminderController;
use App\Http\Controllers\ReminderActionController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route user to the quasar webapp
Route::get('/', function () {
    return view('app');
});

// Get device information
Route::get('/getDevice/{id}','App\Http\Controllers\DataController@getDevice');

Route::get('/getReminder/{id}','App\Http\Controllers\ReminderController@getReminder');

Route::get('/getReminderAction/{id}','App\Http\Controllers\ReminderActionController@getReminderAction');

Route::post('/newReminder','App\Http\Controllers\ReminderController@newReminder');