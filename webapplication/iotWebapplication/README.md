# How to start the application

- Build the quasar project `cd quasarapp` & `quasar build`
  - Don't forget `npm install` if its the first time
- Run `npm run dev` inside this laravel directory
- You're good to go! 