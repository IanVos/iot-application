# Backend application
This application communicates with the mqtt broker to send and receive messages from the iot device. 
When a message is received this application decides what needs to be done with the information.
The application will mostly receive information about the actions a user has done on the iot device.
When a action is communcicated this application will store it in the database so it can be seen in the frond end.

## Mqtt communication
This application is subscribed with the mqtt broker running on the server. 

## Database connection
This application stores action data in the database and reads reminder data from the database.

## Reminder activation
An other important part of this application is to send the reminder activation to the iot device.