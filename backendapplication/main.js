/***
 * @author Ian Vos
 * @date 2020-11-07
 * @description A backend application to communicate with the mqtt broker and send messages to the iot device
 */
const MQTT_HOST = 'mqtt://h2905623.stratoserver.net';   // Set mqtt host to server
const WAIT_INTERVAL = 10;                               // Set wait interval of 60 seconds

/* Load my sql module and connect */
var mysql = require('mysql');
var mysql_connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "iot"
});
mysql_connection.connect(function(err) {
    if (err) throw err;
    console.log("Connected to mysql database");
});


/* Load MQTT module and connect to the host */
var mqtt = require('mqtt');
var client  = mqtt.connect(MQTT_HOST);


client.on('connect', function () {
  client.subscribe('response', function (err) {
    if (!err) {
      console.log('Subscribed to response channel');
    }
  });
  client.subscribe('ignored', function (err) {
    if (!err) {
      console.log('Subscribed to ignored channel');
    }
  });
  client.subscribe('welcome', function (err) {
    if (!err) {
      console.log('Subscribed to welcome channel');
    }
  });
});
 
client.on('message', function (topic, message) {
  // message is Buffer
  console.log(topic.toString() + ' - ' + message.toString())
  if(topic == 'response'){
    storeAction('Reponded in time', message.toString(), 1);
  } else if(topic == 'welcome'){
    console.log('An esp is online');
    welcomeEsp(message.toString());
  } else if(topic == 'ignored'){
    storeAction('Ignored reminder', message.toString(), 0);
  }
})

function welcomeEsp(espWelcomeMessage){
  let espParts = espWelcomeMessage.split('-');
  let sqlQuery = "SELECT * FROM devices WHERE espid = '" + espParts[0] + "';";
  mysql_connection.query(sqlQuery, function (err, result) {
    if (err) throw err;
    if(result.length == 0){
      let sqlQuery = "INSERT INTO devices (espid, user) VALUES (" + espParts[0] + ",'" + espParts[1] + "')";
      mysql_connection.query(sqlQuery, function (err, result) {
        if (err) throw err;
      });
    } 
  });
}

/***
 * Store a new action in the database
*/
function storeAction(message, reminderId, state){
  if(reminderId == ''){
    return;
  }
  let sqlQuery = "INSERT INTO reminder_actions (reminder_id, message, state) VALUES (" + reminderId + ",'" + message + "', " + state + " )";
  mysql_connection.query(sqlQuery, function (err, result) {
    if (err) throw err;
  });
}


/***
 * Get date time function
 * Get the date and time with the addition of the param seconds
 */
function getDateTime(addSeconds){
  let date_ob = new Date();
  date_ob.setSeconds(date_ob.getSeconds() + (addSeconds - 1));
  let day = ("0" + date_ob.getDate()).slice(-2);
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  let year = date_ob.getFullYear();
  let hours = date_ob.getHours();
  let minutes = date_ob.getMinutes();
  let seconds = date_ob.getSeconds();
  return (year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds);
}

/***
 * Get the reminders that need to be activaded from the datavase
 */
function getCurrentReminders(){
  let sqlQuery = "SELECT * FROM reminders WHERE time between '" + getDateTime(0) + "' AND '" + getDateTime(WAIT_INTERVAL) + "';";
  mysql_connection.query(sqlQuery, function (err, result) {
    if (err) throw err;
    result.forEach(reminder => {
      console.log(reminder);
      console.log((reminder.espid + '-' + reminder.prio + '-' + reminder.id));
      client.publish('reminders', (reminder.espid + '-' + reminder.prio + '-' + reminder.id));
    });
  });
}

/***
 * Main logic loop
 * Runs every WAIT_INTERVAL seconds
 * Checks the reminders and activats them when needed
 */
function main(){
  getCurrentReminders();


}

console.log('Starting...')
// use: setInterval / setTimeout if this blocks mqtt listen
setInterval(main, WAIT_INTERVAL * 1000);